#!/usr/bin/env bash

# X X * * * cd /root/docker/xxx; ./backup.sh >> /root/backup.log

#			* Local Backup
#			BACKUP_PATH=pv/backup
#			BACKUP_EX=--exclude=backup
# バックアップ先の領域として
# mkdir -pv pv/backup; chown -v 1000:1000 pv/backup しておくこと
#			* Remote Backup
#			BACKUP_HOST=user@rsync_ssh_host
#			BACKUP_PATH=backup
#			BACKUP_EX=--exclude=id_rsa*
# バックアップ先への ssh アクセスのため
# ssh-keygen -f pv/id_rsa; chown -v 1000:1000 pv/id_rsa しておくこと

		if [ -v BACKUP_PATH ]; then
#			if [ `date +%s` -ge $target1 ]; then
#				((target1 += CYCLE1))
				echo "`date`: Job easy cron 1 started."
				stamp=`date +%Y%m%d_%H%M`
				ssh_opt='-o StrictHostKeyChecking=no -o PasswordAuthentication=no'
				if [ -v BACKUP_HOST ]; then
					coron=:
					ssh_opr="ssh $ssh_opt $BACKUP_HOST"
				fi
				last_backup=`$ssh_opr ls $BACKUP_PATH | tail -1`
				backup_ex0='--exclude=log'
				if [ -z "$last_backup" ]; then
					gen=0
				else
					gen=$((`$ssh_opr cat $BACKUP_PATH/$last_backup/.gen` + 1))
					link_dest="--link-dest=../$last_backup"
				fi
				rsync -av --delete $backup_ex0 $BACKUP_EX -e "ssh $ssh_opt" pv $BACKUP_HOST$coron$BACKUP_PATH/$stamp $link_dest
				echo $gen > .gen; scp $ssh_opt .gen $BACKUP_HOST$coron$BACKUP_PATH/$stamp
#			fi
		fi

