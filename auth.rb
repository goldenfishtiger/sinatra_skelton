use Rack::Auth::Basic, 'Authorization Required' do |username, password|

	authorized = false

	if(@configs[:auth_htpasswd])
		ht_password = false
		open(@configs[:htpasswd_file]) {|fh|
			fh.each {|l|
				l =~ /^#{username}:(.+)/ and ht_password = $1 and break
			}
		}
		if((it = ht_password) and it =~ /^{SHA}(.+)/i)
			require 'digest/sha1'
			hash_base64 = $1
			hash = hash_base64.unpack('m*')[0]
			challenge = Digest::SHA1.digest(password)
			authorized |= (hash == challenge)
		end
	end

	if(@configs[:auth_ldap])
		require 'net/ldap'
		ldap_password = false
		ldap = Net::LDAP.new(
			:host	=> @configs[:ldap_host],
			:port	=> @configs[:ldap_port],
			:auth	=> @configs[:ldap_auth],
		)
		results = ldap.search(
			:base		=> @configs[:ldap_search_base],
			:filter		=> '(cn=%s)' % username,
			:attributes	=> ['userpassword'],
		)
		results or raise(ldap.get_operation_result.to_s)
		(it = results) and (it = it[0]) and (it = it[:userpassword]) and (it = it[0]) and ldap_password = it
		if((it = ldap_password) and it =~ /^{SSHA}(.+)/i)
			require 'digest/sha1'
			hash_salt_base64 = $1
			hash_salt = hash_salt_base64.unpack('m*')[0]
			hash = hash_salt[0, 20]
			salt = hash_salt[20, 4]
			challenge = Digest::SHA1.digest(password + salt)
			authorized |= (hash == challenge)
		end
	end

	authorized
end

__END__

